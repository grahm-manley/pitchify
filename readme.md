# Pitchify
[Pitchify](https://play.google.com/store/apps/details?id=com.grahmmanley.pitchify&hl=en) 
is an android application which displays the Pitchfork.com review for
the Spotify album you are currently playing. It relies on the backend api 
[PitchScrape](https://bitbucket.org/grahm-manley/pitchscrape-ruby).

