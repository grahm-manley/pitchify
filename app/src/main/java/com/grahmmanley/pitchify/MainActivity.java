package com.grahmmanley.pitchify;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.support.customtabs.CustomTabsIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {
    // Graphical elements
    TextView albumTitleBox;
    TextView artistsBox;
    LinearLayout notFoundHeader;
    TextView notFoundText;
    Button openSpotifyButton;
    ImageView albumArt;
    CardView pitchforkCard;
    TextView pitchforkAbstract;

    boolean broadcastReceived;
    Album album; // Current album
    Handler handler; // Timer
    CustomTabsIntent customTabsIntent; // In-app browser

    /** Instantiate receiver, retrieve UI elements, and check
     * if broadcast are being received */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        broadcastReceived = false;
        handler = new Handler();

        // Register Broadcast Receiver
        SpotifyBroadcastReceiver r = new SpotifyBroadcastReceiver();
        IntentFilter i = new IntentFilter();
        i.addAction("com.spotify.music.metadatachanged");
        registerReceiver(r,i);

        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        customTabsIntent = builder.build();

        // Get view fields
        this.albumTitleBox = findViewById(R.id.albumTitle);
        this.artistsBox = findViewById(R.id.artists);
        this.pitchforkCard = findViewById(R.id.pitchforkCard);
        this.pitchforkAbstract = findViewById(R.id.pitchforkAbstract);
        this.albumArt = findViewById(R.id.albumArt);
        this.notFoundHeader = findViewById(R.id.notFoundHeader);
        this.notFoundText = findViewById(R.id.notFoundText);
        this.openSpotifyButton = findViewById(R.id.openSpotify);

        // Check if broadcast are being received after 7 seconds
        handler.postDelayed(new Runnable(){
           public void run() {
               checkBroadcast();
           }
        }, 7000);

    }

    /** Check whether broadcasts are being received and notify user if not */
    public void checkBroadcast() {
        if (!broadcastReceived){
            this.notFoundText.setText("It appears Spotify isn't currently playing. " +
                    "If Spotify is playing you may need to turn " +
                    "on Device Broadcast Status in the Spotify app's settings");
            this.notFoundHeader.setVisibility(View.VISIBLE);
            this.albumArt.setVisibility(View.INVISIBLE);
            this.pitchforkCard.setVisibility(View.INVISIBLE);
            this.openSpotifyButton.setVisibility(View.VISIBLE);

            Log.d("Main", "No broadcast received");
        }
        else {
            Log.d("Main", "Broadcast received");
        }
    }

    /** Send user to Spotify app */
    public void openSpotify(View view) {
        PackageManager manager = this.getApplicationContext().getPackageManager();
        Intent intent = manager.getLaunchIntentForPackage("com.spotify.music");
        startActivity(intent);
    }

    /** Update current album to given album and update UI */
    public void updateAlbum(Album album) {
        if(this.album != null && album.getTitle().equals(this.album.getTitle())) {
            return;
        }
        this.album = album;
        this.albumTitleBox.setText(album.getTitle());
        this.artistsBox.setText(album.getArtists()[0]);
    }

    /** Open in app browser to display current album url */
    public void openReview(View view) {
        customTabsIntent.launchUrl(this, Uri.parse(album.getLink()));
    }

    /** Notify that an album was received from broadcast and retrieved
     *  from the API Updates UI elements */
    public void success() {
        this.notFoundHeader.setVisibility(View.INVISIBLE);
        Picasso.get().load(album.getImageUrl()).into(this.albumArt);
        this.albumArt.setVisibility(View.VISIBLE);
        this.pitchforkCard.setVisibility(View.VISIBLE);
        this.pitchforkAbstract.setText(album.getSummary());
        broadcastReceived = true;
    }

    /** Notify that an album was received from broadcast but
     * could not be found from API request */
    public void failed() {
        this.notFoundText.setText("Could not find any information on currently playing album");
        this.notFoundHeader.setVisibility(View.VISIBLE);
        this.albumArt.setVisibility(View.INVISIBLE);
        this.pitchforkCard.setVisibility(View.INVISIBLE);
        this.openSpotifyButton.setVisibility(View.INVISIBLE);
        broadcastReceived = true;
    }
}
