package com.grahmmanley.pitchify;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class SpotifyBroadcastReceiver extends BroadcastReceiver {
    static final class BroadcastTypes {
        static final String SPOTIFY_PACKAGE = "com.spotify.music";
        static final String METADATA_CHANGED = SPOTIFY_PACKAGE + ".metadatachanged";
    }

    /** Receives Spotify album status and updates UI by
     * notifying MainActivity */
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null && action.equals(BroadcastTypes.METADATA_CHANGED)) {
            String artistName = intent.getStringExtra("artist");
            String albumName = intent.getStringExtra("album");
            MainActivity mainActivity = (MainActivity) context;
            String[] artists = {artistName};
            mainActivity.updateAlbum(new Album(albumName, artists, mainActivity));
        }
    }
}
