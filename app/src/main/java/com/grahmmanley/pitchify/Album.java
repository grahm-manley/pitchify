package com.grahmmanley.pitchify;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

public class Album {
    private MainActivity mainActivity;
    private String title;
    private String link;
    private String summary;
    private String imageUrl;
    private String[] artists;
    private double score;
    private RequestQueue requestQueue;

    Album(String title, String[] artists,  MainActivity context) {
        requestQueue = Volley.newRequestQueue(context);
        this.mainActivity = context;
        this.title = title;
        this.artists = artists;
        getAlbumInfo();
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public String[] getArtists() {
        return artists;
    }

    public String getLink() {
        return link;
    }

    public String getSummary() {
       return summary;
    }

    public double getScore() {
        return score;
    }

    private void getAlbumInfo() {
        String url = "http://pitchscrape.grahmmanley.com/albums/";
        url = url + title;

        for(int i = 0; i < artists.length; i++) {
            if(i == 0) {
                url = url + "/?artists[]=" + artists[0];
            }
            else{
                url = url + "&artists[]=" + artists[i];
            }
        }
        Log.d("url", url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject attributes = response.getJSONObject("data").getJSONObject("attributes");
                            JSONArray reviews = attributes.getJSONArray("reviews");
                            JSONObject review = (JSONObject) reviews.get(0);
                            JSONObject scoreObj = (JSONObject) attributes.getJSONArray("scores").get(0);
                            score = scoreObj.getDouble("value");
                            link = review.getString("link");
                            summary = review.getString("abstract");
                            imageUrl = attributes.getString("image-url");
                            mainActivity.success();
                        } catch (JSONException e) {
                            Log.d("json error", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("album", "error");
                        Log.e("album", error.toString());
                        mainActivity.failed();
                    }
                });
        requestQueue.add(jsonObjectRequest);
    }
}
